#pragma once
#include "Drive.h"
#include "Drive_Statement.h"

extern "C" NTSTATUS DriverEntry(PDRIVER_OBJECT drive_object, PUNICODE_STRING path)
{
	DbgPrint("Start...\n");
	drive_object->DriverUnload = DriverUnload;
	DbgPrint("Wating..\n");
	Jump_Flags(drive_object);

	Register_Load_Image();
	DbgPrint("Load_Image..\n");


	return STATUS_SUCCESS;
}

void DriverUnload(PDRIVER_OBJECT drive_object)
{
	UNREFERENCED_PARAMETER(drive_object);
	Unload__Load_Image();


	DbgPrint("Unload Over!\n");
}

void ShuDownWrite()
{
	_asm
	{
		cli;
		mov eax, cr0;
		and eax, 0FFFEFFFFh; //and eax,0FFFEFFFFh
		mov cr0, eax;
	}

}

void StartWrite()
{
	_asm
	{
		mov eax, cr0;
		or  eax, 10000h;
		mov cr0, eax;
		sti;
	}
}

void Jump_Flags(PDRIVER_OBJECT drive_object)
{
	PLDR_DATA_TABLE_ENTRY ldr;
	ldr = (PLDR_DATA_TABLE_ENTRY)drive_object->DriverSection;
	ldr->Flags |= 0x20;
}