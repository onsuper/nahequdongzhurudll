#pragma once
#include <Ntifs.h>




void DriverUnload(PDRIVER_OBJECT drive_object);
void ShuDownWrite();
void StartWrite();
void Jump_Flags(PDRIVER_OBJECT drive_object);
NTSTATUS Register_Load_Image();
VOID Load_Image(
	_In_ PUNICODE_STRING FullImageName,
	_In_ HANDLE ProcessId,                // pid into which image is being mapped
	_In_ PIMAGE_INFO ImageInfo
	);
NTSTATUS Unload__Load_Image();
void Write_Process_Memory();



/// <summary>
/// Get exported function address
/// </summary>
/// <param name="pBase">Module base</param>
/// <param name="name_ord">Function name or ordinal</param>
/// <param name="pProcess">Target process for user module</param>
/// <param name="baseName">Dll name for api schema</param>
/// <returns>Found address, NULL if not found</returns>
PVOID BBGetModuleExport(IN PVOID pBase, IN PCCHAR name_ord);